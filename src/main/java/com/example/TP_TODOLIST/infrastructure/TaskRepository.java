package com.example.TP_TODOLIST.infrastructure;

import com.example.TP_TODOLIST.domain.Task;
import com.example.TP_TODOLIST.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findByUser(User user);
    Task findByCode(String code);
}
