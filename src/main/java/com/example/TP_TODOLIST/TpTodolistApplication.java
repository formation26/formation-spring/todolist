package com.example.TP_TODOLIST;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpTodolistApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpTodolistApplication.class, args);
	}

}
