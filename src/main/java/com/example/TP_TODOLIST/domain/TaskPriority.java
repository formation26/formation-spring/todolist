package com.example.TP_TODOLIST.domain;

public enum TaskPriority {
    LOW,
    MEDIUM,
    HIGH
}
