package com.example.TP_TODOLIST.domain;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;



    @Entity
    @Table(name = "person")
    public class User {

        @Id
        @Email
        @NotEmpty
        @Column(unique = true)
        private String email;
        @NotEmpty
        @NotNull
        private String name;
        @Size(min = 8)
        private String password;
        @OneToMany(mappedBy = "user", cascade = {CascadeType.MERGE,CascadeType.PERSIST})
        private List<Task> tasks;

        public User() {
        }

        public User(String email, String name, String password, List<Task> tasks) {
            this.email = email;
            this.name = name;
            this.password = password;
            this.tasks = tasks;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public List<Task> getTasks() {
            return tasks;
        }

        public void setTasks(List<Task> tasks) {
            this.tasks = tasks;
        }
    }
