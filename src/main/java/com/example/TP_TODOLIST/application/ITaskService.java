package com.example.TP_TODOLIST.application;

import com.example.TP_TODOLIST.domain.Task;
import com.example.TP_TODOLIST.domain.TaskPriority;
import com.example.TP_TODOLIST.domain.TaskStatus;
import com.example.TP_TODOLIST.domain.User;
import com.example.TP_TODOLIST.dto.TaskDetailDto;
import com.example.TP_TODOLIST.dto.TaskDto;
import com.example.TP_TODOLIST.dto.UserDetailDto;
import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface ITaskService {

    /**
     * Finds all tasks in the system and returns their details in a list of task detail DTOs.
     *
     * @return a list of task detail DTOs containing the details of all tasks in the system
     */
    List<TaskDto> findAll();

    /**
     * Creates a new task with the specified task.
     *
     * @param task the new task
     * @return the created task
     * @throws IllegalArgumentException if any of the specified details is invalid
     */
    TaskDetailDto createTask(TaskDetailDto task) throws IllegalArgumentException;

    /**
     * Update specific task with the specified task.
     *
     * @param updatedTask the new task
     * @return the updated task
     * @throws IllegalArgumentException if any of the specified details is invalid
     */
    TaskDetailDto updateTask(TaskDetailDto updatedTask) throws IllegalArgumentException;

    /**
     * Delete specific task with the specified id.
     *
     * @param  id the identifier of the task to be deleted
     * @throws EntityNotFoundException If task doesn't exist
     */
    void deleteTask(Long id) throws EntityNotFoundException;

    /**
     * Get specific task with the specified id.
     *
     * @param  code the functionnal code of the task to be deleted
     * @throws EntityNotFoundException If task doesn't exist
     */
    TaskDetailDto findTaskByCode(String code);

    /**
     * Sets the specified task as completed.
     *
     * @param code the functionnal code of the task to be completed
     * @throws EntityNotFoundException If task doesn't exist
     */
    void setTaskCompleted(String code) throws IllegalArgumentException;

    /**
     * Changes the status of the specified task to the specified new status.
     *
     * @param task the task whose status should be changed
     * @throws IllegalArgumentException if the specified task or new status is not valid
     * @throws EntityNotFoundException If task doesn't exist
     */
    void changeTaskStatus(TaskDto task) throws IllegalArgumentException, EntityNotFoundException ;

    /**
     * Changes the priority of the specified task to the specified new priority.
     *
     * @param task the task whose priority should be changed
     * @param newPriority the new priority for the task
     * @throws IllegalArgumentException if the specified task or new priority is not valid
     * @throws EntityNotFoundException If task doesn't exist
     */
    void changeTaskPriority(TaskDto task,TaskPriority newPriority) throws IllegalArgumentException, EntityNotFoundException ;

    /**
     * Finds all tasks belonging to the specified user.
     *
     * @param user the user whose tasks should be found
     * @return a list of tasks belonging to the specified user
     * @throws EntityNotFoundException if the specified user does not exist or does not have any tasks
     */
    List<TaskDto> findTasksByUser(UserDetailDto user) throws EntityNotFoundException;

    /**
     * Assigns the specified user to the specified task.
     *
     * @param code the functionnal code of the task to be assigned to the user
     * @param user the user to be assigned to the task
     * @throws IllegalArgumentException if the specified task or user is not valid
     */
    void assignUserToTask(String code, UserDetailDto user);
}
