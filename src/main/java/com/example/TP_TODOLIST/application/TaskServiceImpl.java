package com.example.TP_TODOLIST.application;

import com.example.TP_TODOLIST.domain.Task;
import com.example.TP_TODOLIST.domain.TaskPriority;
import com.example.TP_TODOLIST.domain.TaskStatus;
import com.example.TP_TODOLIST.domain.User;
import com.example.TP_TODOLIST.dto.TaskDetailDto;
import com.example.TP_TODOLIST.dto.TaskDto;
import com.example.TP_TODOLIST.dto.UserDetailDto;
import com.example.TP_TODOLIST.infrastructure.TaskRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskServiceImpl implements ITaskService{

//Declaration de variable


    private TaskRepository taskRepository;

    @Autowired
    private ModelMapper modelMapper;


    //Blocs
    @Autowired
    public  TaskServiceImpl(TaskRepository taskRepository) {
      this.taskRepository=taskRepository;
    }
    @Override
    public List<TaskDto> findAll() {

        return taskRepository.findAll()
                .stream()
                .map(task -> modelMapper.map(task, TaskDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public TaskDetailDto createTask(TaskDetailDto taskDto) throws IllegalArgumentException {
        Task taskToCreate=modelMapper.map(taskDto, Task.class);
        Task taskEntity=taskRepository.save(taskToCreate);
        return modelMapper.map(taskEntity, TaskDetailDto.class);
    }

    @Override
    public TaskDetailDto updateTask(TaskDetailDto updatedTask) throws IllegalArgumentException {
        Task task = taskRepository.findByCode(updatedTask.getCode());

        task.setName(updatedTask.getName());
        task.setDescription(updatedTask.getDescription());
        task.setStartDate(updatedTask.getStartDate());
        task.setEndDate(updatedTask.getEndDate());
        task.setPriority(updatedTask.getPriority());

        return modelMapper.map(taskRepository.save(task), TaskDetailDto.class);
    }

    @Override
    public void deleteTask(Long id) throws EntityNotFoundException {
        taskRepository.deleteById(id);
    }

    @Override
    public TaskDetailDto findTaskByCode(String code) {
        return modelMapper.map(taskRepository.findByCode(code),TaskDetailDto.class);
    }

    @Override
    public void setTaskCompleted(String code) throws IllegalArgumentException {
        Task task = taskRepository.findByCode(code);
         task.setCompleted(true);
        task.setStatus(TaskStatus.COMPLETED);
        taskRepository.save(task);
    }

    @Override
    public void changeTaskStatus(TaskDto task) throws IllegalArgumentException, EntityNotFoundException {

       /*
        task.setStatus(newStatus);
        taskRepository.save(modelMapper.map(task,Task.class));
      */
        Task taskEntity = taskRepository.findByCode(task.getCode());
        taskEntity.setStatus(task.getStatus());
        taskRepository.save(taskEntity);

    }

    @Override
    public void changeTaskPriority(TaskDto taskDto, TaskPriority newPriority) throws IllegalArgumentException, EntityNotFoundException {
        Task task = taskRepository.findByCode(taskDto.getCode());
        task.setCompleted(true);
        taskRepository.save(task);
    }

    @Override
    public List<TaskDto> findTasksByUser(UserDetailDto user) throws EntityNotFoundException {

        return taskRepository.findByUser(modelMapper.map(user,User.class))
                .stream()
                .map(task -> modelMapper.map(task, TaskDto.class))
                .collect(Collectors.toList());
    }


    @Override
    public void assignUserToTask(String code , UserDetailDto userDto) {

        Task task = taskRepository.findByCode(code);
        task.setUser(modelMapper.map(userDto,User.class));
        taskRepository.save(task);
    }
}
