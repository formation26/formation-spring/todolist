package com.example.TP_TODOLIST.exposition;

import com.example.TP_TODOLIST.application.ITaskService;
import com.example.TP_TODOLIST.domain.TaskPriority;
import com.example.TP_TODOLIST.domain.TaskStatus;
import com.example.TP_TODOLIST.dto.TaskDetailDto;
import com.example.TP_TODOLIST.dto.TaskDto;
import com.example.TP_TODOLIST.dto.UserDetailDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/task")
public class TaskController {

    ITaskService taskService;
    public TaskController(ITaskService service){
           this.taskService=service;
    }

    @GetMapping("/all")
    public ResponseEntity<List<TaskDto>> listTasks() {

        return ResponseEntity.ok(taskService.findAll());

    }

    @PostMapping("/new")
    public ResponseEntity<TaskDetailDto> createTask(@RequestBody TaskDetailDto task) {


        return ResponseEntity.ok(taskService.createTask(task));
    }


    @PutMapping("/edit")
    public ResponseEntity<TaskDetailDto> updateTask(@RequestBody TaskDetailDto dto) {

        return ResponseEntity.ok(taskService.updateTask( dto));

    }

    @PutMapping("/edit/status")
    public void updateTaskStatus(@RequestBody TaskDto dto) {

         taskService.changeTaskStatus( dto);

    }

    @PutMapping("/edit/priority/{priority}")
    public void updateTaskPriority(@RequestBody TaskDto dto, @PathVariable("priority") TaskPriority priority) {

       taskService.changeTaskPriority(dto,priority);

    }

    @GetMapping("/delete/{id}")
    public void deleteTask(@PathVariable Long id){
        taskService.deleteTask(id);

    }

    @GetMapping("/markDone/{code}")
    public void setTaskCompleted(@PathVariable String code){
        taskService.setTaskCompleted(code);

    }

    @PutMapping("/assign")
   public void assignUser(String code, UserDetailDto user){
        taskService.assignUserToTask(code,user);
   }
}
