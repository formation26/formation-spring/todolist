package com.example.TP_TODOLIST.dto;

import com.example.TP_TODOLIST.domain.Task;

import java.util.List;

public class UserDetailDto {


    private String email;

    private String name;

    private String password;
    private List<Task> tasks;
}
