package com.example.TP_TODOLIST.dto;

import com.example.TP_TODOLIST.domain.TaskPriority;
import com.example.TP_TODOLIST.domain.TaskStatus;


public class TaskDto {

    private String name;

    private String code;

    private String description;

    private boolean isCompleted;

    private TaskPriority priority;
    private TaskStatus status;

    public TaskDto(String name,String code, String description, boolean isCompleted, TaskPriority priority, TaskStatus status) {
        this.name = name;
        this.code = code;
        this.description = description;
        this.isCompleted = isCompleted;
        this.priority = priority;
        this.status = status;
    }

    public TaskDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public TaskPriority getPriority() {
        return priority;
    }

    public void setPriority(TaskPriority priority) {
        this.priority = priority;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
