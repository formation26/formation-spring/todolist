package com.example.TP_TODOLIST.dto;

import com.example.TP_TODOLIST.domain.Category;
import com.example.TP_TODOLIST.domain.TaskPriority;
import com.example.TP_TODOLIST.domain.TaskStatus;
import com.example.TP_TODOLIST.domain.User;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class TaskDetailDto {
    private String name;
    private String code;
    private String description;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate startDate;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate endDate;
    private boolean isCompleted;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate deadline;
    private TaskPriority priority;
    private TaskStatus status;
    private User user;
    private Category category;

    public TaskDetailDto(String name, String code, String description, LocalDate startDate, LocalDate endDate, boolean isCompleted, LocalDate deadline, TaskPriority priority, TaskStatus status, User user, Category category) {
        this.name = name;
        this.code = code;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isCompleted = isCompleted;
        this.deadline = deadline;
        this.priority = priority;
        this.status = status;
        this.user = user;
        this.category = category;
    }

    public TaskDetailDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public TaskPriority getPriority() {
        return priority;
    }

    public void setPriority(TaskPriority priority) {
        this.priority = priority;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}


